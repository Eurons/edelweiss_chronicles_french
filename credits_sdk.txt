﻿>Enterbrain
Moteur de base de Pokémon SDK, le RGSS 3.

>Game Freak & Nintendo
Univers Pokémon.

>Aerun
Programmation de certaines attaques, diverses suggestions, participation à l’intégration des movesets des Pokémon.
Correction de la position des Pokémon

>Jayzon
Réalisation de l'animation d'entrée en combat des dresseurs.

>Overworlds des générations 1, 2, 3, 4 et 5 : 
MissingLukey, help-14, Kymotonian, cSc-A7X, 2and2makes5, Pokegirl4ever, Fernandojl, Silver-Skies, TyranitarDark, Getsuei-H, Kid1513, Milomilotic11, Kyt666, kdiamo11, Chocosrawlooid, Syledude, Gallanty, Gizamimi-Pichu, 2and2makes5, Zyon17

>Overworlds de la génération 6 : Princess-Phoenix, LunarDusk6

>Credits Sprites 6G SDK :
Diegotoon20
Domino99designs
Falgaia
GeoisEvil
Juan-Amador
N-Kin
Noscium
SirAquaKip
Smogon XY Sprite Project
Zermonious
Zerudez

>Crédits Formes Alola
Smogon

>Otaku
Divers aides graphiques (interface) / Certains Shiny Alola

>Angi-MK, Aethnight & la communauté de Gemme
Participation de près ou de loin au projet.

>Pαlвσlѕку, Qwerty, Tokeur, Redder, Schneitizel, Mauduss
Participation à l'élaboration de la base de données.

>Fafa, Jarakaa, Shamoke, BigOrN0t, Bouzouw, Diaband.onceron, Kiros97, Likton, MrSuperluigis, oldu49, SMB64, sylvaintr, UltimaShayra & Unbreakables
Bêta-test de Pokémon SDK au travers de Pokémon Gemme. (4G)

>Speed / SirMalo
Ressources graphiques originales de PSDK.

>Solfay
Diverses interfaces basiques de PSDK.

>Aye, Anti-NT
Suggestions.

>Cayrac
Programmation d'attaques

>Krosk
Fondateur et développeur de Pokémon Script Project (projet abandonné).

>Smogon, X-Act, Peterko & Kaphotics
Diverses formules et extraction des textes de Pokémon.

>Nuri Yuri
Programmation du FollowMe
Programmation du moteur de la base de données
Programmation du système de combats
Programmation des modules complémentaires de Pokémon SDK
Chef de projet de Pokémon SDK
Mise à jour BDD 7G mineures

>Poképedia, Otaku
Icones 7G

>Don
Correction des offset des battlers de Pokémon

>Asia81
Textes Sun & Moon

>Leikt
Interrupteur Locaux à distance
MAPG (Plugin)
Game Self Variables (Variables locales)
Escaliers améliorés

>FL0RENT_
Ruines Alpha (Puzzle)

>AEliso19
Mise à jour BDD 7G (objets, Pokémon)
Fichier ball_17 (ultra ball)

>Neslug
Animation Shiny

>Renkys
Interface du système de quêtes

>Sprites (Front/Back) 7G :
Amethyst, Jan, Zumi, Bazaro, Koyo, Smeargletail, Alex, Noscium, Lepagon, N-kin, fishbowlsoul90, princess-phoenix, DatLopunnyTho, Conyjams , kaji atsu , The cynical poet, LuigiPlayer, Falgaia of the Smogon S/M sprite project, Pikafan2000, Lord-Myre

>Animation PSP :
Metaiko (Création des animations)
Isomir (Animation Close Combat)
KLNOTHINCOMIN (Animation Aéroblast, Eco-Sphere et Fulmigraine)
Bulbapedia (Liste des attaques avec les animations)